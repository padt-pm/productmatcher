#%%
from annoy import AnnoyIndex
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse


parser = argparse.ArgumentParser(description='Accuracy for created embeddins')
parser.add_argument('-d', '--dataset', default='sampleData.csv', type=str,
                    help='product dataset *.csv')
parser.add_argument('-t', '--tree', default='tree.ann', type=str,
                    help='annoy tree *.ann')
parser.add_argument('-s', '--embedding_size', default='1024', type=int,
                    help='vector embedding size for')

args = parser.parse_args()
dataset = args.dataset
tree = args.tree
embedding_size = args.embedding_size

df = pd.read_csv(dataset, index_col=0)
ean_all = df["EAN"]
with_single_eans = ean_all.dropna()
ean = with_single_eans[with_single_eans.duplicated(keep=False)]
u = AnnoyIndex(embedding_size, 'angular')
u.load(tree)

# %%
all = len(ean.index)
k_neighbour = 10
product_pos_in_results = np.zeros(k_neighbour)
for i in ean.index:
    results = u.get_nns_by_item(i, k_neighbour+1)
    if i in results:
        results.remove(i)
    else:
        results = results[:-1]
    suggested_eans = [ean_all[r_inx] for r_inx in results]
    try:
        pos = suggested_eans.index(ean_all[i])
        product_pos_in_results[pos] += 1
    except ValueError:
        pass
acc_topK = np.cumsum(product_pos_in_results) / all
print(f"Top 1 accuracy: {acc_topK[0]}")
print(f"Top 5 accuracy: {acc_topK[4]}")

plt.plot(acc_topK,  marker='o')
plt.ylabel('Accuracy')
plt.xlabel('Closest K offers')
plt.ylim(0, 1)
plt.show()

