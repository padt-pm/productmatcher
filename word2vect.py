#%%
from laserembeddings import Laser
import sys
import pandas as pd
import numpy as np
import argparse

parser = argparse.ArgumentParser(description='Transform input column to embeddings.')
parser.add_argument('-i', '--input_file', default='sampleData.csv', type=str,
                    help='*.csv input file')
parser.add_argument('-c', '--column_name', default='title', type=str,
                    help='name of column in [inputFile.csv]')
parser.add_argument('-o', '--output_file', default='out_embedding.npy', type=str,
                    help='*.npy output file')

args = parser.parse_args()

inputFile = args.input_file
columnName = args.column_name
outputFile = args.output_file

if not inputFile.endswith(".csv"):
    inputFile += ".csv"

df = pd.read_csv(inputFile, index_col=0)
sentences = df[columnName]

laser = Laser()

embeddings = laser.embed_sentences(sentences, lang='pl')
with open(outputFile, "wb") as f:
    np.save(f, embeddings)

# %%
