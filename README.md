# USAGE
pip install annoy (might throw a C++ error -> Install C++ package using Visual Studio installer and try again)
pip install laserembeddings
python -m laserembeddings download-models

## 1. Transform input column to embeddings.

Optional arguments:

| COMMAND                                   | DESCRIPTION                       |
|:------------------------------------------|:----------------------------------|
| -h, --help                                | show this help message and exit   | 
| -i INPUT_FILE, --input_file INPUT_FILE    | *.csv input file                  |  
| -c COLUMN_NAME, --column_name COLUMN_NAME | name of column in [inputFile.csv] |  
| -o OUTPUT_FILE, --output_file OUTPUT_FILE | *.npy output file                 |
## 2. Build annoy tree.

Optional arguments:

| COMMAND                                   | DESCRIPTION                       |
|:------------------------------------------|:----------------------------------|
| -h, --help                                | show this help message and exit   | 
| -e EMBEDDINGS, --embeddings EMBEDDINGS    | product embeddings                |  
| -o TREE_OUTPUT, --tree_output TREE_OUTPUT | output tree file *.ann            |

## 3a. Calculate accuracy.

Optional arguments:

| COMMAND                                            | DESCRIPTION                       |
|:---------------------------------------------------|:----------------------------------|
| -h, --help                                         | show this help message and exit   | 
| -d DATASET, --dataset DATASET                      | product dataset *.csv             |
| -t TREE, --tree TREE                               | output file with annoy tree *.ann |  
| -s EMBEDDING_SIZE, --embedding_size EMBEDDING_SIZE | vector embeddings size def=1024   |  
## 3b. Single prediction.

Optional arguments:

| COMMAND                                            | DESCRIPTION                       |
|:---------------------------------------------------|:----------------------------------|
| -h, --help                                         | show this help message and exit   | 
| -d DATASET, --dataset DATASET                      | product dataset *.csv             |
| -t TREE, --tree TREE                               | output file with annoy tree *.ann |
| -s EMBEDDING_SIZE, --embedding_size EMBEDDING_SIZE | vector embeddings size def=1024   |
| -n SIMILAR_OUTPUT, --similar_output SIMILAR_OUTPUT | return n most similar products    | 
