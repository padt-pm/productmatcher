#%%
import numpy as np
import argparse
from annoy import AnnoyIndex


parser = argparse.ArgumentParser(description='Prediction for product id.')
parser.add_argument('-e', '--embeddings', default='out_embedding.npy', type=str,
                    help='product embeddings')
parser.add_argument('-o', '--tree_output', default='tree.ann', type=str,
                    help='output tree file *.ann')

args = parser.parse_args()
embeddings = args.embeddings
tree_output = args.tree_output


embeddings  = np.load(embeddings)

trees = 10 # more trees more accurate query
f = len(embeddings[1]) #vector length
t = AnnoyIndex(f, 'angular')

for idx, v in enumerate(embeddings):
     t.add_item(idx, v)

t.build(trees) 
t.save(tree_output)
