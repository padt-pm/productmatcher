#%%
from annoy import AnnoyIndex
import pandas as pd
import argparse


parser = argparse.ArgumentParser(description='Prediction for product id.')
parser.add_argument('-i', '--product_id', default='0', type=int,
                    help='product id from dataset')
parser.add_argument('-d', '--dataset', default='sampleData.csv', type=str,
                    help='product dataset *.csv')
parser.add_argument('-t', '--tree', default='tree.ann', type=str,
                    help='annoy tree *.ann')
parser.add_argument('-s', '--embedding_size', default='1024', type=int,
                    help='vector embedding size')
parser.add_argument('-n', '--similar_output', default='20', type=int,
                    help='return n most similar products')

args = parser.parse_args()
product_id = args.product_id
dataset = args.dataset
tree = args.tree
embedding_size = args.embedding_size
similar_output = args.similar_output

u = AnnoyIndex(embedding_size, 'angular')
u.load(tree)

df = pd.read_csv(dataset, index_col=0)
titles = df["title"]
desc = df["description"]
ean = df["EAN"]

results = u.get_nns_by_item(product_id, similar_output)

print("Input product")
print(f"id: {product_id}, EAN: {ean[product_id]}, title: {titles[product_id]}, description {titles[product_id]}")

print("Matches")
for r in results:
    if r == product_id:
        print(f"Product from query: id: {r}, EAN: {ean[r]}, title: {titles[r]}, description: {titles[r]}")
    else:
        print(f"id: {r}, EAN: {ean[r]}, title: {titles[r]}, description: {titles[r]}")